# Made for Python 2

import datetime, time, requests, json, os, sys, pickle

userName = 'BGS'
avatar = 'https://i.imgur.com/EbIV5eJ.png'

######################################################################################################

# Data for Feedback contact (Edit as you wish)
feedbackWebhook = "https://discordapp.com/api/webhooks/*********/***************"

try:
    systemName = sys.argv[1]
    webHookURL = sys.argv[2]
    try:
        pingDest = sys.argv[3]
    except:
        pingDest = ""
except:
    response = requests.post(feedbackWebhook, json={"username": userName, "avatar_url": avatar,
                                                    "content": "One of the CronJobs is not correctly set."})
    raise Exception("No arguments given")


def feedback(_):
    try:
        response = requests.post(feedbackWebhook, json={"username": userName, "avatar_url": avatar, "content": str(_)})
    except:
        print(_)

cooldowns = {}

try:
    with open("system_cooldowns.json", "rb") as raw_cooldowns:
        cooldowns = pickle.load(raw_cooldowns)
    if cooldowns.get(systemName, 0) > time.time():
        raise Exception("In cooldown")
except Exception as e:
    if str(e) == "In cooldown" and not os.path.exists("pForceRun_" + systemName + ".txt"):
        raise Exception("In cooldown")

try:
    r = requests.get('https://www.edsm.net/api-system-v1/factions?systemName=' + systemName + '&showHistory=1')
    if r == None:
        raise Exception("No EDSM response")
    r = r.json()

    if os.path.exists("pForceRun_" + systemName + ".txt"):
        ForceRun = True
    else:
        ForceRun = False

    contentWH = {}
    preOutput = {}
    factionsData = []
    prevInf = {}
    prevState = {}
    sysID = r['id']
    sysID64 = r['id64']
    sysName = r['name']
    sysUrl = r['url']
    sysContrFacName = r['controllingFaction']['name']
    sysFactionCount = len(r['factions'])
    sysFactionInfUnchanged = 0
    sysFactionStateUnchanged = 0
    if sysFactionCount == 0:
        raise Exception("System has no factions")

    for sysFaction in r['factions']:
        factionData = {}
        pendingStateTemp = 'None'
        for pendingStateIndex in sysFaction['pendingStates']:
            pendingStateTempTrend = ''
            if pendingStateIndex['trend'] != 0:
                pendingStateTempTrend = ' ^'
            if pendingStateTemp == 'None':
                pendingStateTemp = ''
                pendingStateTemp = pendingStateIndex['state'] + pendingStateTempTrend
            else:
                pendingStateTemp = pendingStateTemp + ', ' + pendingStateIndex['state'] + pendingStateTempTrend
        recoveringStateTemp = 'None'
        for recoveringStateIndex in sysFaction['recoveringStates']:
            recoveringStateTempTrend = ''
            if recoveringStateIndex['trend'] != 0:
                recoveringStateTempTrend = ' v'
            if recoveringStateTemp == 'None':
                recoveringStateTemp = ''
                recoveringStateTemp = recoveringStateIndex['state'] + recoveringStateTempTrend
            else:
                recoveringStateTemp = recoveringStateTemp + ', ' + recoveringStateIndex[
                    'state'] + recoveringStateTempTrend

        lastTimestamp = 0
        lastStatestamp = 0
        oldFactionData = {}
        for infHist in sysFaction['influenceHistory']:
            if lastTimestamp < int(infHist):
                lastTimestamp = int(infHist)
        for stateHist in sysFaction['stateHistory']:
            if lastStatestamp < int(stateHist):
                lastStatestamp = int(stateHist)

        factionData = {'id': int(sysFaction['id']),
                       'name': str(sysFaction['name']),
                       'allegiance': str(sysFaction['allegiance']),
                       'government': str(sysFaction['government']),
                       'influence': float("%04.1f" % (sysFaction['influence'] * 100)),
                       'state': str(sysFaction['state']),
                       'pendingStates': str(pendingStateTemp),
                       'recoveringStates': str(recoveringStateTemp),
                       'lastTimestamp': int(lastTimestamp),
                       'lastStatestamp': int(lastStatestamp),
                       'isPlayer': bool(sysFaction['isPlayer'])
                       }
        print("=================")
        print(sysFaction['name'])
        try:
            with open("FactionData/pFD_" + sysName + "_" + str(factionData['id']) + ".json", "rb") as raw_data:
                oldFactionData = pickle.load(raw_data)
            prevInf[str(sysFaction['id'])] = "%04.1f" % (oldFactionData['influence'])
            prevState[str(sysFaction['id'])] = str(oldFactionData['state'])
        except Exception as e:
            print("No previous data found - Prev Inf check")
            print (e)
            prevInf[str(sysFaction['id'])] = 0
            prevState[str(sysFaction['id'])] = ""

        try:
            print ("Stored data: " + str(oldFactionData['influence']) + " - " + str(
                oldFactionData['state']))
        except:
            pass
        print ("Latest data: " + str(factionData['influence']) + " - " + str(factionData['state']))
        print ("")

        try:
            if float(oldFactionData['influence']) == float(factionData['influence']):
                sysFactionInfUnchanged += 1
                print("No Inf changes " + str(sysFactionInfUnchanged) + "/" + str(sysFactionCount))

            if oldFactionData['state'] == factionData['state']:
                sysFactionStateUnchanged += 1
                print("No State changes " + str(sysFactionStateUnchanged) + "/" + str(sysFactionCount))
            if (float(oldFactionData['influence']) == float(factionData['influence'])) and (
                    oldFactionData['state'] == factionData['state']):
                if sysFactionInfUnchanged + sysFactionStateUnchanged >= sysFactionCount * 2 and not ForceRun:
                    raise Exception("NoChanges")
        except Exception as e:
            if "NoChanges" == str(e):
                raise Exception("Same data, stopping execution")

        if not os.path.exists("FactionData"):
            os.makedirs("FactionData")

        with open("FactionData/pFD_" + sysName + "_" + str(factionData['id']) + ".json", "wb") as f:
            pickle.dump(factionData, f)
        factionsData.append(factionData.copy())

    print("=================")

    preOutput['factionFullContent'] = ""
    preOutput['factionStatesContent'] = ""

    nameFPadding = 29
    statePendingFPadding = 14
    stateRecoveringFPadding = 17
    stateFPadding = 13
    for factionItem in factionsData:
        nameFPaddingToAdd = 0
        stateFPaddingToAdd = 0

        if factionItem['isPlayer'] == True:
            nameFPaddingToAdd = 1
        if str(prevState[str(factionItem['id'])]) == "":
            nameFPaddingToAdd += 1
        if nameFPadding < len(factionItem['name']) + nameFPaddingToAdd:
            nameFPadding = len(factionItem['name']) + nameFPaddingToAdd

        if str(factionItem['state']) != str(prevState[str(factionItem['id'])]):
            stateFPaddingToAdd = 1
        if stateFPadding < len(factionItem['state']) + stateFPaddingToAdd:
            stateFPadding = len(factionItem['state']) + stateFPaddingToAdd

        if statePendingFPadding < len(factionItem['pendingStates']):
            statePendingFPadding = len(factionItem['pendingStates'])
        if stateRecoveringFPadding < len(factionItem['recoveringStates']):
            stateRecoveringFPadding = len(factionItem['recoveringStates'])

    lastInfDataUpdate = 0
    lastStateDataUpdate = 0
    for factionItem in r['factions']:
        for infHist in factionItem['influenceHistory']:
            if lastInfDataUpdate < int(infHist):
                lastInfDataUpdate = int(infHist)
        for stateHist in factionItem['stateHistory']:
            if lastStateDataUpdate < int(stateHist):
                lastStateDataUpdate = int(stateHist)
    lastInfDataUpdate = int(lastInfDataUpdate)
    lastInfDataUpdateFormatted = datetime.datetime.utcfromtimestamp(lastInfDataUpdate).strftime('%Y-%m-%d %H:%M')
    lastStateDataUpdate = int(lastStateDataUpdate)
    lastStateDataUpdateFormatted = datetime.datetime.utcfromtimestamp(lastStateDataUpdate).strftime('%Y-%m-%d %H:%M')

    for factionItem in factionsData:
        factionNameTemp = ""
        if factionItem['isPlayer'] == True:
            factionNameTemp = "*" + factionItem['name']
        else:
            factionNameTemp = factionItem['name']
        if str(prevState[str(factionItem['id'])]) == "":
            factionNameTemp = "!" + factionNameTemp

        tempInf = "%04.1f" % (float(factionItem['influence']))
        prevInfTemp = "%04.1f" % (float(prevInf[str(factionItem['id'])]))

        # If the previous influence and the current one are both 0, it will skip it from the results.
        try:
            if float(factionItem['influence']) == 0 and float(prevInf[str(factionItem['id'])]) == 0:
                print("Skipping {}, still at influence 0").format(factionNameTemp)
                continue
        except:
            pass

        try:
            if prevInfTemp > tempInf:
                tempDiff = float(prevInfTemp) - float(tempInf)
                tempInfStr = "-" + "%04.1f" % (float(tempDiff)) + "% - " + tempInf
                tempInfPartialStr = "%04.1f" % (float(factionItem['influence']))
            elif prevInfTemp == tempInf:
                tempInfStr = "@00.0% - " + tempInf
                tempInfPartialStr = "%04.1f" % (float(factionItem['influence']))
            else:
                tempDiff = float(tempInf) - float(prevInfTemp)
                tempInfStr = "+" + "%04.1f" % (float(tempDiff)) + "% - " + tempInf
                tempInfPartialStr = "%04.1f" % (float(factionItem['influence']))
        except Exception as e:
            tempInfStr = "+" + tempInf + "% - " + tempInf
            tempInfPartialStr = "%04.1f" % (float(factionItem['influence']))

        tempState = str(factionItem['state'])
        prevStateTemp = str(prevState[str(factionItem['id'])])

        tempPenStateStr = factionItem['pendingStates']
        tempRecStateStr = factionItem['recoveringStates']

        try:
            if prevStateTemp != tempState:
                tempStateStr = "!" + tempState
            else:
                tempStateStr = tempState
        except Exception as e:
            tempStateStr = tempState

        if int(factionItem['influence']) == 0:
            tempInfStr = "-~~~~~ - ~~~~"
            tempInfPartialStr = "~~~~"
            tempStateStr = "Retreated"
            tempPenStateStr = "~" * statePendingFPadding
            tempRecStateStr = "~" * stateRecoveringFPadding

        # Output for both types of changes, Inf and States
        if preOutput['factionFullContent'] == "":
            preOutput['factionFullContent'] = "Change - C.Inf. | Faction (* = Player, ! = New)" + " " * (
                        nameFPadding - 29) + " | Current State" + " " * (
                                                          stateFPadding - 13) + " | Pending States" + " " * (
                                                          statePendingFPadding - 14) + " | Recovering States"
            preOutput['factionFullContent'] = preOutput[
                                                  'factionFullContent'] + "\n________________|______________________________" + "_" * (
                                                          nameFPadding - 29) + "_|______________" + "_" * (
                                                          stateFPadding - 13) + "_|_______________" + "_" * (
                                                          statePendingFPadding - 14) + "_|__________________"

        preOutput['factionFullContent'] = preOutput[
                                              'factionFullContent'] + "\n" + tempInfStr + " % | " + factionNameTemp.ljust(
            nameFPadding) + " | " + tempStateStr.ljust(stateFPadding)
        preOutput['factionFullContent'] = preOutput['factionFullContent'] + " | " + tempPenStateStr.ljust(
            statePendingFPadding)
        preOutput['factionFullContent'] = preOutput['factionFullContent'] + " | " + tempRecStateStr

        # Output for only State Changes
        if preOutput['factionStatesContent'] == "":
            preOutput['factionStatesContent'] = "C.Inf. | Faction (* = Player, ! = New)" + " " * (
                        nameFPadding - 29) + " | Current State" + " " * (
                                                            stateFPadding - 13) + " | Pending States" + " " * (
                                                            statePendingFPadding - 14) + " | Recovering States"
            preOutput['factionStatesContent'] = preOutput[
                                                    'factionStatesContent'] + "\n_______|______________________________" + "_" * (
                                                            nameFPadding - 29) + "_|______________" + "_" * (
                                                            stateFPadding - 13) + "_|_______________" + "_" * (
                                                            statePendingFPadding - 14) + "_|__________________"

        preOutput['factionStatesContent'] = preOutput[
                                                'factionStatesContent'] + "\n" + tempInfPartialStr + " % | " + factionNameTemp.ljust(
            nameFPadding) + " | " + tempStateStr.ljust(stateFPadding)
        preOutput['factionStatesContent'] = preOutput['factionStatesContent'] + " | " + tempPenStateStr.ljust(
            statePendingFPadding)
        preOutput['factionStatesContent'] = preOutput['factionStatesContent'] + " | " + tempRecStateStr

    if pingDest == "here":
        pingText = " - @here"
    elif pingDest == "everyone":
        pingText = " - @everyone"
    elif pingDest != "":
        pingText = " - <@" + pingDest + ">"
    else:
        pingText = ""

    if ForceRun:
        print("Force running " + sysName)
    if sysFactionInfUnchanged < sysFactionCount or ForceRun:
        precontent = '[**' + sysName + '**](<' + sysUrl + '>)' + pingText + '\n```diff\n' + preOutput[
            'factionFullContent'] + '```*Last Influence update on: ' + lastInfDataUpdateFormatted + ' UTC - Last States update on: ' + lastStateDataUpdateFormatted + ' UTC\nData extracted from [EDSM](https://www.edsm.net)*'
    elif sysFactionStateUnchanged < sysFactionCount:
        precontent = '[**' + sysName + '**](<' + sysUrl + '>)' + pingText + '\n```diff\n' + preOutput[
            'factionStatesContent'] + '```*Last States update on: ' + lastStateDataUpdateFormatted + ' UTC - Data extracted from [EDSM](https://www.edsm.net)*'
    else:
        raise Exception("Something went wrong when writting the content")

    contentWH = {"username": userName, "avatar_url": avatar, "content": precontent}

    response = requests.post(webHookURL, json={"username": contentWH["username"], "avatar_url": contentWH["avatar_url"],
                                               "content": contentWH["content"]})
    # Calculating cooldown and setting it.
    try:
        with open("system_cooldowns.json", "rb") as raw_cooldowns:
            cooldowns = pickle.load(raw_cooldowns)
    except:
        pass
    cooldowns[systemName] = time.time() + 21500
    with open("system_cooldowns.json", "wb") as raw_cooldowns:
        cooldowns = pickle.dump(cooldowns, raw_cooldowns)

except Exception as e:
    _ = ('Error on line {} - {} - {}'.format(sys.exc_info()[-1].tb_lineno, type(e).__name__, e))
    print(_)
