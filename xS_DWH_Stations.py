# Made for Python 2

import datetime, time, requests, json, os, sys, pickle

userName = 'BGS'
avatar = 'https://i.imgur.com/EbIV5eJ.png'

######################################################################################################

# Data for Feedback contact (Edit as you wish)
feedbackWebhook = "https://discordapp.com/api/webhooks/*****************/******************"

try:
    systemName = sys.argv[1]
    webHookURL = sys.argv[2]
    try:
        pingDest = sys.argv[3]
    except:
        pingDest = ""
except:
    response = requests.post(feedbackWebhook, json={"username": userName, "avatar_url": avatar,
                                                    "content": "One of the CronJobs is not correctly set."})
    raise Exception("No arguments given")


def feedback(_):
    try:
        response = requests.post(feedbackWebhook, json={"username": userName, "avatar_url": avatar, "content": str(_)})
    except:
        print(_)

cooldowns = {}

try:
    with open("station_cooldowns.json", "rb") as raw_cooldowns:
        cooldowns = pickle.load(raw_cooldowns)
    if cooldowns.get(systemName, 0) > time.time():
        raise Exception("In cooldown")
except Exception as e:
    if str(e) == "In cooldown" and not os.path.exists("pForceRun_" + systemName + ".txt"):
        raise Exception("In cooldown")

try:
    r = requests.get('https://www.edsm.net/api-system-v1/stations?systemName=' + systemName)
    if r == None:
        raise Exception("No EDSM response")
    r = r.json()

    if os.path.exists("pForceRun_" + systemName + ".txt"):
        ForceRun = True
    else:
        ForceRun = False

    contentWH = {}
    preOutput = {}
    preOutput['stationOutput'] = "**Station Ownership report:**"
    stationsData = []
    prevOwner = {}
    oldStationData = {}
    prevState = {}
    sysID = r['id']
    sysID64 = r['id64']
    sysName = r['name']
    sysUrl = r['url']
    sysStationCount = len(r['stations'])
    sysFactionOwnerUnchanged = 0
    if sysStationCount == 0:
        raise Exception("System has no stations")

    for sysStation in r['stations']:
        stationData = {'id': int(sysStation['id']),
                       'name': str(sysStation['name']),
                       'type': str(sysStation['type']),
                       'marketId': int(sysStation['marketId']),
                       'distanceToArrival': float(sysStation['distanceToArrival']),
                       'allegiance': str(sysStation['allegiance']),
                       'government': str(sysStation['government']),
                       'economy': str(sysStation['economy']),
                       'secondEconomy': str(sysStation['secondEconomy']),
                       'haveMarket': bool(sysStation['haveMarket']),
                       'haveShipyard': bool(sysStation['haveShipyard']),
                       'haveOutfitting': bool(sysStation['haveOutfitting']),
                       'otherServices': str(sysStation['otherServices']),
                       'controllingFactionName': str(sysStation['controllingFaction']['name']),
                       'controllingFactionId': int(sysStation['controllingFaction']['id']),
                       #'lastTimestamp': int(sysStation['updateTime']['information']),
                       #'lastStatestamp': int(lastStatestamp),
                       }
        print("=================")
        print(sysStation['name'])
        try:
            with open("StationData/pFD_" + sysName + "_" + str(stationData['id']) + ".json", "rb") as raw_data:
                oldStationData = pickle.load(raw_data)
            prevOwner[str(sysStation['id'])] = oldStationData['controllingFactionName']
        except Exception as e:
            print("No previous data found - Prev Owner check")
            oldStationData['controllingFactionName'] = "Unknown"
            oldStationData['controllingFactionId'] = 0
            print (e)
            prevOwner[str(sysStation['id'])] = 'None'

        try:
            print ("Stored data: " + str(oldStationData['controllingFactionName']))
        except:
            pass
        print ("Latest data: " + str(stationData['controllingFactionName']))
        print ("")

        try:
            if oldStationData['controllingFactionName'] == stationData['controllingFactionName']:
                sysFactionOwnerUnchanged += 1
                continue
        except Exception as e:
            pass

        if not os.path.exists("StationData"):
            os.makedirs("StationData")

        with open("StationData/pFD_" + sysName + "_" + str(stationData['id']) + ".json", "wb") as f:
            pickle.dump(stationData, f)
        stationsData.append(stationData.copy())

        print("=================")
    
        preOutput['stationOutput'] = preOutput['stationOutput']+"\n[*"+ str(stationData['name']) +"*](<https://www.edsm.net/en/system/stations/id/"+str(sysID)+"/name/"+systemName+"/details/idS/"+str(stationData['id'])+"/>) " \
                                    "has changed ownership from " \
                                    "[*"+oldStationData['controllingFactionName']+"*](<https://www.edsm.net/en/faction/id/"+str(oldStationData['controllingFactionId'])+"/name/>)" \
                                    " to [*"+stationData['controllingFactionName']+"*](<https://www.edsm.net/en/faction/id/"+str(stationData['controllingFactionId'])+"/name/>)."

    if pingDest == "here":
        pingText = " - @here"
    elif pingDest == "everyone":
        pingText = " - @everyone"
    elif pingDest != "":
        pingText = " - <@" + pingDest + ">"
    else:
        pingText = ""

    if ForceRun:
        print("Force running " + sysName)
    if sysFactionOwnerUnchanged < sysStationCount or ForceRun:
        precontent = '[**' + sysName + '**](<' + sysUrl + '>)' + pingText + '\n\n' + preOutput['stationOutput'] + ''
    else:
        raise Exception("Same data, stopping execution")

    contentWH = {"username": userName, "avatar_url": avatar, "content": precontent}

    response = requests.post(webHookURL, json={"username": contentWH["username"], "avatar_url": contentWH["avatar_url"],
                                               "content": contentWH["content"]})
    # Calculating cooldown and setting it.
    try:
        with open("station_cooldowns.json", "rb") as raw_cooldowns:
            cooldowns = pickle.load(raw_cooldowns)
    except:
        pass
    cooldowns[systemName] = time.time() + 21500
    with open("station_cooldowns.json", "wb") as raw_cooldowns:
        cooldowns = pickle.dump(cooldowns, raw_cooldowns)

except Exception as e:
    _ = ('Error on line {} - {} - {}'.format(sys.exc_info()[-1].tb_lineno, type(e).__name__, e))
    print(_)
